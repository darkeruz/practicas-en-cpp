/*
4.- Programar la clase Cola (tipo de dato abstracto FIFO) mediante una lista enlazada
que contenga números de DNIs de personas. Luego instancie una cola vacía y
mediante un menú permita añadir un elemento (push) y sacar un elemento (pop)
según lo determine el usuario.
(hasta 9 puntos)
*/
using namespace std;

#include <iostream>
#include <string>
#include <sstream>
#include <unistd.h>

//Creamos la clase Nodo
class Nodo{
private:
    string dni;
    Nodo *link;


public:
    Nodo(){
        this-> dni = "";
        this->link = NULL;
    }
    Nodo(string dni, Nodo *link){
        this->dni = dni;
        this->link = link;
    }


    void setDni(string dni){this->dni = dni;}
    void setLink(Nodo *link){this->link = link;}
    Nodo *getLink(){return link;}
    string getDni(){return dni;}
    ~Nodo(){
    }

};

class Cola{
private:
    Nodo *puntero_primero;

public:
    Cola(){
        puntero_primero = NULL;
    }



    void ingresar_elemento(string dni, Nodo *){
        puntero_primero = new Nodo(dni, puntero_primero);
    }

    void quitar_elemento(Nodo *){
      Nodo *auxiliar = puntero_primero;
        if(auxiliar){
            while(auxiliar->getLink()){
                auxiliar = auxiliar->getLink();
            }
        }else{
            cout<<"Lista vacia."<<endl;
        }

        delete auxiliar->getLink();
        }


        string toString(){
            Nodo *auxiliar = puntero_primero;
            stringstream s;
            while(auxiliar != NULL){
                s<<"DNI: "<<auxiliar->getDni()<<" -->";
                auxiliar = auxiliar->getLink();
            }
            return s.str();
        }

};

int main(int argc, char argv){
    int opcion=0;
    string dni;
    Nodo *nodo1;
    Cola *cola1 = new Cola();


    do{
        cout<<":::MENU:::"<<endl;
        cout<<"1) Ingresa un dni a la cola."<<endl;
        cout<<"2) Quita un dni de la cola."<<endl;
        cout<<"3) Mostrar todos los dni."<<endl;
        cout<<"4) Salir."<<endl;
        cout<<"Elige una opcion: "<<endl;
        cin>>opcion;

        switch(opcion){
            case 1:{
                cout<<":::INGRESAR UN DNI:::"<<endl<<endl;
                cout<<"ingrese un dni: "<<endl;
                cin>>dni;
                nodo1 = new Nodo;

                cola1->ingresar_elemento(dni, nodo1->getLink());

                cout<<"Dni ingresado a la cola con exito"<<endl;
                sleep(2);
                system("clear");


            }break;

            case 2:{
                cout<<":::QUITAR UN DNI DE LA COLA:::"<<endl<<endl;
                cola1->quitar_elemento(nodo1);

                 cout<<"Elemento eliminado de la cola con exito"<<endl;
                sleep(2);
                system("clear");

            }break;

            case 3:{
                cout<<cola1->toString()<<endl;

            }break;

            case 4:{

            }break;

            default:{
                cout<<"La opcion ingresada no es correcta."<<endl;
            }

        }




    }while(opcion !=4);




}
