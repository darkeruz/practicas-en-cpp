/*
3.- Implementar una clase Conjunto, cuyos elementos sean números enteros menores
a 100. Los objetos instanciados de esta clase, deberán contener el número de
elementos que posee y tendrán la posibilidad de realizar la siguientes operaciones:
a) vaciar el conjunto
b) agregar un elemento
c) eliminar un elemento
d) unión con otro conjunto
e) intersección con otro conjunto


*/


#include <iostream>
#include <string>
#include <sstream>
#include <unistd.h>

using namespace std;

//Creamos la clase Nodo
class Nodo{
private:
    int numero;
    Nodo *link;

public:
    //metodo constructor
    Nodo(){
        numero = 0;
        link = NULL;
    }

    //metodo constructor sobrecargado
    Nodo(int numero, Nodo *link){
        this->numero = numero;
        this->link = link;
    }

    //Getters and setters
    int getNumero(){return numero;}
    void setNumero(int numero){this->numero = numero;}
    Nodo *getLink(){return link;}
    void setLink(Nodo *link){this->link = link;}

    //Destructor
    ~Nodo(){
    }

};

//Creamos la clase Conjunto que funcionara como una lista enlazada de nodos.
class Conjunto{
private:
    Nodo *puntero_primero;

public:
    //Constructor
    Conjunto(){
        puntero_primero = NULL;
    }



    void vaciar_conjunto(){
        Nodo *auxiliar;
        while(puntero_primero){
            auxiliar = puntero_primero;
            puntero_primero = puntero_primero->getLink();
            delete auxiliar;
        }
    }

    void agregar_elemento(int numero){

        Nodo *auxiliar = puntero_primero;
        Nodo *nuevo;
        if(auxiliar == NULL){
            puntero_primero = new Nodo(numero, puntero_primero);
        }else{
            while(auxiliar->getLink() != NULL){
                    auxiliar = auxiliar->getLink();
                }
            nuevo = new Nodo(numero, NULL);
            auxiliar->setLink(nuevo);
            }
    }

    void eleminar_elemento(int numero){
        Nodo *auxiliar = puntero_primero;
        Nodo *auxiliar2;
        int x = 1;
        if(auxiliar != NULL){
            if(numero == 1){
                puntero_primero = auxiliar->getLink();
            }else{
                while(x < numero && auxiliar->getLink() != NULL){
                    auxiliar2 = auxiliar;
                    auxiliar = auxiliar->getLink();
                    x++;
                }
                auxiliar2->setLink(auxiliar->getLink());
            }
            delete auxiliar;
        }

    }

    void union_con_otro_conjunto(Conjunto *conjunto2){
        Nodo *auxiliar = puntero_primero;
        if(auxiliar){
            while(auxiliar->getLink()){
                if(auxiliar->getNumero() == conjunto2)

                auxiliar = auxiliar->getLink();
            }
        }else{
            cout<<"Lista vacia."<<endl;
        }

        cout<<"El ultimo nodo es: \n"<<auxiliar->getPersona()->toString()<<endl;

    }

    void interseccion_con_otro_conjunto(Conjunto *conjunto2){
        Nodo *auxiliar = puntero_primero;
        if(auxiliar){
            while(auxiliar->getLink()){
                if(auxiliar->getNumero() == conjunto2)

                auxiliar = auxiliar->getLink();
            }
        }else{
            cout<<"Lista vacia."<<endl;
        }

        cout<<"El ultimo nodo es: \n"<<auxiliar->getPersona()->toString()<<endl;


        }


};
