#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Auto{
private:
    string marca;
    string color;
    float precio;

public:
    //Constructor
    Auto(){
        marca = " ";
        color = " ";
        precio = 0.00;
    }

    //Constructor sobrecargado
    Auto(string _marca, string _color, float _precio){
        marca = _marca;
        color = _color;
        precio = _precio;
    }

    //Destructor de objetos
    ~Auto(){
        cout<<"\nSe ha destruido el objeto"<<endl;
    }

    //Getters and Setters
    string getMarca(){return marca;}
    string getColor(){return color;}
    float getPrecio(){return precio;}
    void setMarca(string _marca){marca = _marca;}
    void setColor(string _color){color = _color;}
    void setPrecio(float _precio){precio = _precio;}

    string toString(){
        stringstream s;
        s<<"Marca-----: "<<marca<<endl;
        s<<"Color-----: "<<color<<endl;
        s<<"Precio----: "<<precio<<endl;

        return s.str();
    }
};

class Vector{
private:
    Auto *vect[10];
    int cantidad;
    int tamanio;

public:
    //Constructor
    Vector();

    //Destructor
    ~Vector();

    void ingresarAuto(Auto *);

    string toString();
};


int main(){
    Auto *auto1 = new Auto("Ferrari", "Negro", 316000.00);
    Auto *auto2 = new Auto("Peugeot", "Azul", 150000.00);
    Auto *auto3 = new Auto("VMW", "Rojo", 234000.50);
    Auto *auto4 = new Auto("Fiat", "Blanco", 100000.12);

    Vector *vector1 = new Vector;


    vector1->ingresarAuto(auto1);
    vector1->ingresarAuto(auto2);
    vector1->ingresarAuto(auto3);
    vector1->ingresarAuto(auto4);
    cout<<vector1->toString();

    delete auto1, auto2, auto3, auto4, vector1;

    return 0;
}

Vector::Vector(){
    cantidad = 0;
    tamanio = 10;
    for(int i = 0; i < tamanio; i++){
        vect[i] = NULL;
    }
}

void Vector::ingresarAuto(Auto *a){
    if(cantidad < tamanio){
        vect[cantidad++] = a;
    }else{
        cout<<"No hay espacio en el vector"<<endl;
    }

}

string Vector::toString(){
    stringstream s;
    for(int i = 0; i < cantidad; i++){
        s<<vect[i]->toString()<<endl;
    }
    return s.str();
}

Vector::~Vector(){
    for(int i = 0; i < cantidad; i++){
        delete vect[i];
    }
    delete []vect;
}
