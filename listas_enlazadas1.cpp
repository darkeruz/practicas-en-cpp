#include <iostream>
#include <string>
#include <sstream>

using namespace std;

//Creamos la clase persona
class Persona{
    private:
        string _nombre;
        string _apellido;
        int _edad;


    public:
        //Metodo constructor
        Persona(){
            _nombre = " ";
            _apellido = " ";
            _edad = 0;
        }

        //Metodo constructor sobrecargado
        Persona(string nombre, string apellido, int edad){
            _nombre = nombre;
            _apellido = apellido;
            _edad = edad;
        }

        //Getter and Setters
        string getNombre(){
            return _nombre;
        }

        string getApellido(){
            return _apellido;
        }

        int getEdad(){
            return _edad;
        }

        ~Persona(){

        }

        //Metodo toString es para imprimir por pantalla los datos de una persona.
        string toString(){
            stringstream s;
            s<<"Nombre-------: "<<_nombre<<endl;
            s<<"Apellido-----: "<<_apellido<<endl;
            s<<"Edad---------: "<<_edad<<endl;
            return s.str();
        }


};

//Creamos la clase Nodo
class Nodo{
private:
    Persona *persona;
    Nodo *link;


public:
    Nodo();
    Nodo(Persona *, Nodo *);
    void setPersona(Persona *);
    void setLink(Nodo *);
    Nodo *getLink();
    Persona *getPersona();
    ~Nodo();



};

class Lista{
private:
    Nodo *puntero_primero;

public:
    Lista();
    void ingresa_persona(Persona *);
    string toString();
    void ingresa_persona_al_ultimo(Persona *);
    void elimina_nodo(int);
    void elimina_nodo_por_apellido(string);
    void ordenar_lista_por_edad();
    void buscar_ultimo_nodo();
    Nodo *buscar_nodo(string);
    void ingresar_nodo_segun_orden(Persona *);
    ~Lista();


};

int main(){
    Lista *lista1 = new Lista();
    Persona *persona1 = new Persona("David", "Cabral", 32);
    Persona *persona2 = new Persona("Roberto", "Quinteros", 42);
    Persona *persona3 = new Persona("Lalo", "Landa", 316);
    Persona *persona4 = new Persona("Martin", "Chimiliwinsky", 30);
    Persona *persona5 = new Persona("Ines", "Basso", 29);
    Persona *persona6 = new Persona("Marianela", "Quinteros", 23);
    Persona *persona7 = new Persona("Darkeruz", "DarkMatter", 32);

    lista1->ingresa_persona(persona1);
    lista1->ingresa_persona(persona2);
    lista1->ingresa_persona(persona3);
    lista1->ingresa_persona_al_ultimo(persona4);
    lista1->ingresa_persona_al_ultimo(persona5);
    lista1->ingresa_persona_al_ultimo(persona6);

    cout<<lista1->toString()<<endl;
    cout<<endl;
    //lista1->elimina_nodo(4);
    //cout<<lista1->toString()<<endl;
    //lista1->elimina_nodo_por_apellido("Sabatinni");
    //cout<<lista1->toString()<<endl;
    //lista1->ordenar_lista_por_edad();
    //cout<<lista1->toString()<<endl;
    //lista1->buscar_ultimo_nodo();
    //cout<<lista1->buscar_nodo("Basso")->getPersona()->toString();
    lista1->ingresar_nodo_segun_orden(persona7);
    cout<<lista1->toString()<<endl;




    return 0;
}

//Metodo constructor de la clase Nodo
Nodo::Nodo(){
    persona = NULL;
    link = NULL;
}

//Metodo constructor sobrecargado de la clase nodo.
Nodo::Nodo(Persona *_persona, Nodo *_link){
    persona = _persona;
    link = _link;
}

//Getter and Setters
void Nodo::setPersona(Persona *_persona){
    persona = _persona;
}

void Nodo::setLink(Nodo *_link){
    link = _link;
}

Nodo *Nodo::getLink(){return link;}

Persona *Nodo::getPersona(){return persona;}

Nodo::~Nodo(){
}


//Metodo constructor de la clase Lista
Lista::Lista(){
    puntero_primero = NULL;
}

//Metodo para ir ingresando un objeto Persona a un Nodo de nuestra lista. colocandolo primero en la lista.
void Lista::ingresa_persona(Persona *persona){
    puntero_primero = new Nodo(persona, puntero_primero);
}

//Metodo para ir ingresando un objeto Persona a un nodo de nuestra lista. Colocandolo al último en la lista.
void Lista::ingresa_persona_al_ultimo(Persona *persona){
    Nodo *auxiliar = puntero_primero;
    Nodo *nuevo;
    if(auxiliar == NULL){
        ingresa_persona(persona);
    }else{
        while(auxiliar->getLink() != NULL){
                auxiliar = auxiliar->getLink();
        }
        nuevo = new Nodo(persona, NULL);
        auxiliar->setLink(nuevo);
    }
}

//Metodo de la clase Lista para eliminar un determinado Nodo
void Lista::elimina_nodo(int n){
    Nodo *auxiliar = puntero_primero;
    Nodo *auxiliar2;
    int x = 1;
    if(auxiliar != NULL){
        if(n == 1){
            puntero_primero = auxiliar->getLink();
        }else{
            while(x < n && auxiliar->getLink() != NULL){
                auxiliar2 = auxiliar;
                auxiliar = auxiliar->getLink();
                x++;
            }
            auxiliar2->setLink(auxiliar->getLink());
        }
        delete auxiliar;
    }
}

//Metodo de la clase Lista para eliminar un Nodo por el apellido de la persona
void Lista::elimina_nodo_por_apellido(string _apellido){
    Nodo *auxiliar = puntero_primero;
    Nodo *auxiliar2 = NULL;

    if(auxiliar != NULL){
        while(auxiliar != NULL && auxiliar->getPersona()->getApellido() != _apellido){
            auxiliar2 = auxiliar;
            auxiliar = auxiliar->getLink();
        }
        if(auxiliar == NULL){
            cout<<"No se encontraron datos"<<endl<<endl;
        }else{
            if(auxiliar2 == NULL){
                puntero_primero = auxiliar->getLink();
            }else{
                auxiliar2->setLink(auxiliar->getLink());
            }
            delete auxiliar;
        }
    }
}

//Metodo de la clase Lista para ordenar (de manera ascendente) la lista según la edad de las personas en los nodos, mediante el metodo de la burbuja...
void Lista::ordenar_lista_por_edad(){
    Nodo *auxiliar = puntero_primero;
    Nodo *auxiliar2 = NULL;
    Persona *persona;

    while(auxiliar->getLink() != NULL){
        auxiliar2 = auxiliar->getLink();

        while(auxiliar2 != NULL){
            if(auxiliar->getPersona()->getEdad() > auxiliar2->getPersona()->getEdad()){ //cambiar a '<' para ordenar de manera descendente
                persona = auxiliar2->getPersona();
                auxiliar2->setPersona(auxiliar->getPersona());
                auxiliar->setPersona(persona);
            }
            auxiliar2 = auxiliar2->getLink();
        }

        auxiliar = auxiliar->getLink();
    }
}

//Metodo de la clase Lista para buscar y mostrar el ultimo nodo
void Lista::buscar_ultimo_nodo(){
    Nodo *auxiliar = puntero_primero;
    if(auxiliar){
        while(auxiliar->getLink()){
            auxiliar = auxiliar->getLink();
        }
    }else{
        cout<<"Lista vacia."<<endl;
    }

    cout<<"El ultimo nodo es: \n"<<auxiliar->getPersona()->toString()<<endl;

}

//Metodo de la clase Lista para buscar un Nodo segun el apellido de la persona de ese nodo...
Nodo *Lista::buscar_nodo(string s){
    Nodo *auxiliar;
    if(puntero_primero){
        auxiliar = puntero_primero;
        while(auxiliar && s.compare(auxiliar->getPersona()->getApellido()) != 0){
            auxiliar = auxiliar->getLink();
        }return auxiliar;
    }else{
        cout<<"La lista esta vacia."<<endl;
        return NULL;
    }
}

void Lista::ingresar_nodo_segun_orden(Persona *persona){
    Nodo *auxiliar, *nuevo;
    string apellido;
    cout<<"Ingrese el apellido de la persona previa al nuevo ingreso: "<<endl;
    cin>>apellido;
    auxiliar = buscar_nodo(apellido);

    if(!auxiliar){
        ingresa_persona(persona);
    }else{
        cout<<"Nodo encontrado: "<<endl;
        nuevo = new Nodo(persona, auxiliar->getLink());
        //nuevo->setLink(auxiliar->getLink());
        auxiliar->setLink(nuevo);
    }
}

//Metodo toString de la clase Lista, recorrerá la lista e irá mostrando el contenido de cada nodo si lo hubiera.
string Lista::toString(){
    Nodo *auxiliar = puntero_primero;
    stringstream s;
    while(auxiliar != NULL){
        s<<auxiliar->getPersona()->toString()<<endl;
        auxiliar = auxiliar->getLink();
    }
    return s.str();
}

//Metodo destructor de la clase Lista
Lista::~Lista(){
    Nodo *auxiliar = puntero_primero;
    Nodo *actual;
    while(auxiliar != NULL){
        actual = auxiliar->getLink();
        delete auxiliar;
        auxiliar = actual;
    }
    puntero_primero = NULL;
}


















