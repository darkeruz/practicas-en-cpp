#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>

using namespace std;

class Contenedor{
private:
    int _vector[20];
    int _tamanio;
    int _cantidad;

public:
    Contenedor(){
        _tamanio = 15;
        _cantidad = 0;
        for(int i = 0; i < _tamanio; i++){
            _vector[i] = 0;
        }
    }

    void setCantidad(int cantidad){
        _cantidad = cantidad;
    }

    int getCantidad(){
        return _cantidad;
    }

    void solCantidad(){
        int n;
        cout<<"Cuantos datos deseas ingresar? "<<endl;
        cin>>n;
        setCantidad(n);
    }

    void llenar(){
        srand(time(0));
        for(int i = 0; i < _cantidad; i++){
            _vector[i] = semilla(10);
        }
    }

    int semilla(int n){
        return 1 + rand() % n;
    }

    string toString(){
        stringstream s;
        solCantidad();
        llenar();
        for(int i = 0; i < _cantidad; i++){
            s<<_vector[i]<<" ";
        }
        return s.str();
    }

};

int main(){
    Contenedor Con1;
    cout<<Con1.toString()<<endl;

    return 0;
}
