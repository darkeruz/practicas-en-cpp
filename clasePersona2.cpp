#include <iostream>
#include <string>
#include <sstream>
#include <stdlib.h>

using namespace std;

class Persona{
private:
    string apellido;
    string nombre;
    int edad;

public:
    Persona();
    Persona(string, string, int);
    string getApellido();
    string getNombre();
    int getEdad();
    void setApellido(string _apellido);
    void setNombre(string _nombre);
    void setEdad(int _edad);
    void leer();
    void correr();

};

Persona::Persona(){
    apellido = " ";
    nombre = " ";
    edad = 0;
    cout<<"Se ha creado una persona correctamente!"<<endl;
}

Persona::Persona(string _apellido, string _nombre, int _edad){
    apellido = _apellido;
    nombre = _nombre;
    edad = _edad;
    cout<<"Se ha creado una persona mediante el constructor sobrecargado!"<<endl;
}

string Persona::getApellido(){
    return apellido;
}

string Persona::getNombre(){
    return nombre;
}

int Persona::getEdad(){
    return edad;
}

void Persona::setApellido(string _apellido){
    apellido = _apellido;
}

void Persona::setNombre(string _nombre){
    nombre = _nombre;
}

void Persona::setEdad(int _edad){
    edad = _edad;
}

int main(){
    Persona persona1 = Persona();
    Persona persona2 = Persona("Cabral", "David", 32);

    cout<<"Apellido: "<<persona1.getApellido()<<"\nNombre: "<<persona1.getNombre()<<"\nEdad: "<<persona1.getEdad()<<endl;

    string apellido, nombre;
    int edad;

    cout<<"Ingrese el apellido de la persona1: "<<endl;
    cin>>apellido;
    cout<<"Ingrese el nombre de la persona1: " <<endl;
    cin>>nombre;
    cout<<"Ingrese la edad de la persona1: "<<endl;
    cin>>edad;

    persona1.setApellido(apellido);
    persona1.setNombre(nombre);
    persona1.setEdad(edad);

    cout<<"Apellido: "<<persona1.getApellido()<<"\nNombre: "<<persona1.getNombre()<<"\nEdad: "<<persona1.getEdad()<<endl;

    cout<<"Apellido: "<<persona2.getApellido()<<"\nNombre: "<<persona2.getNombre()<<"\nEdad: "<<persona2.getEdad()<<endl;


    return 0;
}
