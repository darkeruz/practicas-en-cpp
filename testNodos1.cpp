#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Nodo{
private:
    string informacion;
    Nodo *link;

public:
    Nodo(){
        informacion = "";
        link = NULL;
    }
    Nodo(string info){
        informacion = info;
        link = NULL;
    }
    void setInformacion(string info){informacion = info;}

    string getInformacion(){return informacion;}

    void setLink(Nodo *_link){link = _link;}

    Nodo *getLink(){return link;}

    //void agregarNodo();

    string toString(){
        stringstream s;
        s<<"La informacion del nodo es: "<<informacion<<endl;
        s<<"El link del nodo es: "<<link<<endl;

        return s.str();
    }

};

int main(int argc, char **argv){
    Nodo nodoA, nodoB;

    string info;
    cout<<"Ingrese la informacion para el primer nodo: "<<endl;
    cin>>info;
    nodoA.setInformacion(info);
    nodoA.setLink(&nodoB);

    cout<<nodoA.toString();

    cout<<"Ingrese informacion para el nodo B: "<<endl;
    cin>>info;
    nodoB.setInformacion(info);


    cout<<nodoB.toString();


    return 0;
}
