#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Persona{
private:
    string apellido;
    string nombre;
    int edad;

public:
    Persona(){
        apellido = "";
        nombre = "";
        edad = 0;
    }

    Persona(string apellido, string nombre, int edad){
        this->apellido = apellido;
        this->nombre = nombre;
        this->edad = edad;
    }

    //Getters and Setters
    string getApellido(){return apellido;}
    void setApellido(string apellido){this->apellido = apellido;}
    string getNombre(){return nombre;}
    void setNombre(string nombre){this->nombre = nombre;}
    int getEdad(){return edad;}
    void setEdad(int edad){this->edad = edad;}

    string toString(){
        stringstream s;
        s<<"Apellido-----: "<<apellido<<endl;
        s<<"Nombre-------: "<<nombre<<endl;
        s<<"Edad---------: "<<edad;
        return s.str();
    }


};

class Nodo{
private:
    Persona *persona;
    Nodo *link;

public:
    Nodo(){
        persona = NULL;
        link = NULL;
    }

    Nodo(Persona *persona){
        this->persona = persona;
    }

    Persona *getPersona(){return persona;}
    void setPersona(Persona *persona){this->persona = persona;}
    Nodo *getLink(){return link;}
    void setLink(Nodo *link){this->link = link;}

    string toString(){
        stringstream s;
        s<<persona->toString()<<endl;
        s<<"Link---: "<<link<<endl;
        cout<<endl;
        return s.str();
    }
};


int main(int argc, char **argv){
    Persona *persona1 = new Persona("Cabral", "David", 32);
    Persona *persona2 = new Persona("Quinteros", "Marianela", 23);
    Nodo *nodo1 = new Nodo();
    Nodo *nodo2 = new Nodo();

    nodo1->setPersona(persona1);
    nodo1->setLink(nodo2);
    nodo2->setPersona(persona2);
    nodo2->setLink(NULL);

    cout<<nodo1->toString();
    cout<<nodo2->toString();


    return 0;
}
