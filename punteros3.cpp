#include <iostream>

using namespace std;

int *cambiar(int *a, int *b){
    *a = 316;
    *b = 316;
    return a;
};


int main(){
    int x = 1, y = 2;

    cout<<"X = "<<x<<" - Y = "<<y<<endl;

    *cambiar(&x, &y);

    cout<<"X = "<<x<<" - Y = "<<y<<endl;

    return 0;
}


