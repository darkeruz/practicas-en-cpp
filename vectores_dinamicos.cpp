#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Auto{
private:
    string marca;
    string color;
    float precio;

public:
    //Constructor
    Auto(){
        marca = " ";
        color = " ";
        precio = 0.00;
    }

    //Constructor sobrecargado
    Auto(string _marca, string _color, float _precio){
        marca = _marca;
        color = _color;
        precio = _precio;
    }

    //Destructor de objetos
    ~Auto(){
        cout<<"";
    }

    //Getters and Setters
    string getMarca(){return marca;}
    string getColor(){return color;}
    float getPrecio(){return precio;}
    void setMarca(string _marca){marca = _marca;}
    void setColor(string _color){color = _color;}
    void setPrecio(float _precio){precio = _precio;}

    string toString(){
        stringstream s;
        s<<"Marca-----: "<<marca<<endl;
        s<<"Color-----: "<<color<<endl;
        s<<"Precio----: "<<precio<<endl;

        return s.str();
    }
};

class Vector{
private:
    Auto *vect;
    int tamanio;
    int cantidad;

public:
    Vector(int);
    void ingresar_auto(Auto);
    string toString();
    ~Vector();


};


int main(){
    Auto auto1("Peugeot", "Rojo", 150000);
    Auto auto2("Fiat", "Blanco", 178000);
    Vector vector1(10);
    vector1.ingresar_auto(auto1);
    vector1.ingresar_auto(auto2);
    cout<<vector1.toString()<<endl;



    return 0;
}

Vector::Vector(int n){
    cantidad = 0;
    tamanio = n;
    vect = new Auto[tamanio];
}

void Vector::ingresar_auto(Auto au){
    if(cantidad < tamanio){
        vect[cantidad++] = au;
    }else{
        cout<<"No hay espacio en el vector."<<endl;
    }
}

string Vector::toString(){
    stringstream s;
    for(int i=0; i<cantidad; i++){
        s<<vect[i].toString()<<endl;
    }
    return s.str();
}

Vector::~Vector(){
    delete []vect;
}



