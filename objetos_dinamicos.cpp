#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Auto{
private:
    string marca;
    string color;
    float precio;

public:
    //Constructor
    Auto(){
        marca = " ";
        color = " ";
        precio = 0.00;
    }

    //Constructor sobrecargado
    Auto(string _marca, string _color, float _precio){
        marca = _marca;
        color = _color;
        precio = _precio;
    }

    //Destructor de objetos
    ~Auto(){
        cout<<"\nSe ha destruido el objeto"<<endl;
    }

    //Getters and Setters
    string getMarca(){return marca;}
    string getColor(){return color;}
    float getPrecio(){return precio;}
    void setMarca(string _marca){marca = _marca;}
    void setColor(string _color){color = _color;}
    void setPrecio(float _precio){precio = _precio;}

    string toString(){
        stringstream s;
        s<<"Marca-----: "<<marca<<endl;
        s<<"Color-----: "<<color<<endl;
        s<<"Precio----: "<<precio<<endl;

        return s.str();
    }
};


int main(){
    Auto *a1 = new Auto("Peugeot", "Rojo", 316000.00);

    cout<<a1->toString();
    delete a1;



    return 0;
}
