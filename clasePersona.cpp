#include <iostream>
#include <string>
#include <sstream>

using namespace std;
class Persona{
    private:
        string _nombre;
        string _apellido;
        int _edad;


    public:
        Persona(){
            _nombre = " ";
            _apellido = " ";
            _edad = 0;
        }

        Persona(string nombre, string apellido, int edad){
            _nombre = nombre;
            _apellido = apellido;
            _edad = edad;
        }

        string getNombre(){
            return _nombre;
        }

        string getApellido(){
            return _apellido;
        }

        int getEdad(){
            return _edad;
        }

        ~Persona(){

        }

        string toString(){
            stringstream s;
            s<<"Nombre-----: "<<_nombre<<endl;
            s<<"Apellido-----: "<<_apellido<<endl;
            s<<"Edad-----: "<<_edad<<endl;
            return s.str();
        }


};

class Contenedor{
private:
    Persona _vector[10];
    int _cantidad;
    int _tamanio;

public:
    Contenedor(){
        _cantidad = 0;
        _tamanio = 10;
    }

    void ingresaPersona(Persona p){
        if(_cantidad < _tamanio){
            _vector[_cantidad++] = p;
        }else{
            cout<<"No hay espacio en el vector..."<<endl;
        }

    }

    string toString(){
        stringstream s;
        for(int i = 0; i < _cantidad; i++){
            s<<_vector[i].toString()<<endl;
        }
        return s.str();
    }

};

int main(){
    Contenedor contenedor1;
    Persona persona1("David", "Cabral", 32);
    Persona persona2("Marianela", "Quinteros", 23);
    Persona persona3("Nahuel", "Roldan", 20);
    contenedor1.ingresaPersona(persona1);
    contenedor1.ingresaPersona(persona2);
    contenedor1.ingresaPersona(persona3);

    cout<<persona1.toString();
    cout<<persona2.toString();
    cout<<persona3.toString();
    cout<<"\nDatos desde el vector."<<endl;
    cout<<contenedor1.toString()<<endl;

    return 0;
}
